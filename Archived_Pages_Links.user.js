// ==UserScript==
// @name     Archived Pages Links
// @description This script provides a additional link (shown as this floppydisk: 💾) to a archived version of the page (or a error if it was never archived). It also adds a InvisiText viewer (👁️‍🗨️).
// @author   Jcc10
// @icon https://web.archive.org/static/images/archive.ico
// @homepageURL https://gitlab.com/Jcc10/Archive_links-Forum_story/tree/master
// @namespace https://www.jcc10.net/gmscripts
// @version  2.0
// @updateURL https://gitlab.com/Jcc10/Archive_links-Forum_story/blob/master/Archived_Pages_Links.user.js.MetaDataOnly
// @downloadURL https://gitlab.com/Jcc10/Archive_links-Forum_story/blob/master/Archived_Pages_Links.user.js
// @grant    none
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @include  https://forums.sufficientvelocity.com/threads/*
// @include  https://forums.spacebattles.com/threads/*
// @include  https://forum.questionablequesting.com/threads/*
// ==/UserScript==

// Converts STR month abbr, to INT month
function getMonth(month){
  // it's just a bunch of cases.
  var numberM = 12;
  
  // just a basic switch case.
  switch(month) {
    case "Jan":
        numberM = 1;
        break;
    case "Feb":
        numberM = 2;
        break;
    case "Mar":
        numberM = 3;
        break;
    case "Apr":
        numberM = 4;
        break;
    case "May":
        numberM = 5;
        break;
    case "Jun":
        numberM = 6;
        break;
    case "Jul":
        numberM = 7;
        break;
    case "Aug":
        numberM = 8;
        break;
    case "Sep":
        numberM = 9;
        break;
    case "Oct":
        numberM = 10;
        break;
    case "Nov":
        numberM = 11;
        break;
    case "Dec":
        numberM = 12;
        break;
    default:
        numberM = 12;
	}
  return numberM ;
}

// gets string day
function getDay(day) {
  var chars = [];
  // this is the lazy approach to removeing the comma.
  if(day.length == 2){
     chars = day.split("");
     return chars[0];
  } else {
     chars = day.split("");
     return chars[0] + "" + chars[1];
  }
}

// gets hour and minute
function getTime(HM, MD) {
  // if something goes wrong, give the last possible hour.
  var out = [23, 59];
  // split it into [H, M]
  var HMP = HM.split(":");
  var H = parseInt(HMP[0]);
  if(MD == "PM"){
    // if it's PM, you need 12 more hours.
    out[0] = H + 12;
  } else {
    out[0] = H;
  }
  out[1] = HMP[1];
  return out ;
}

// this converts the date/time into useable data.
function getDate(strDate){
 // strDate = "Mar 5, 2017 at 2:05 AM"
 //           |0  |1 |2   |3 |4   |5 |
 var parts = strDate.split(" ");
 // while the year may not need padding, the rest do as a safety. (And it looks better all being the same.)
 var yearN = ("" + parts[2]).padStart(4, "0");
 var monthN = ("" + getMonth(parts[0])).padStart(2, "0");
 var dayN = ("" + getDay(parts[1])).padStart(2, "0");
 var HsMs = getTime(parts[4], parts[5]);
 var hourN = ("" + HsMs[0]).padStart(2, "0");
 var minuteN = ("" + HsMs[1]).padStart(2, "0");
  

 // I stick strings in just to make sure it's keeping everything a string... I don't normaly do JS.
 //     2017         07            19          14           59         39
 //     2017         03            05          02           05         00
 return yearN + "" + monthN + "" + dayN + "" + hourN + "" + minuteN + "59" ;
}

// this is for when we have a data-time attribute
function getUnixDate(intDate){
  // this parses the unix timestamp, it's so much easer but does not appear on all posts. :(
  var timestamp = parseInt(intDate);
  var date = new Date(timestamp*1000);

  // See previous function
  var yearN = ("" + date.getFullYear()).padStart(4, "0");
  var monthN = ("" + (date.getMonth() + 1)).padStart(2, "0");
  var dayN = ("" + date.getDate()).padStart(2, "0");
  var hoursN = ("" + date.getHours()).padStart(2, "0");
  var minutesN = ("" + date.getMinutes()).padStart(2, "0");
  var secondsN = ("" + date.getSeconds()).padStart(2, "0");
  
  // See previous function
  //     2017         07            19          14            59              39
  //     2017         03            05          02            05              00
  return yearN + "" + monthN + "" + dayN + "" + hoursN + "" + minutesN + "" + secondsN ;
}

// Returns if a string is absolute or not
function isAbsolute(text) {
  // RegEx for deciding if a link is Absolute or not.
  var RegExTest = new RegExp('^(?:[a-z]+:)?//', 'i');
  return RegExTest.test(text);
  
}

// this adds the archive links
function archiveAdd(baseElement, currentDate) {
  // this is the old link
  var oldLink = baseElement.attr('href');

  // this ensures that reletive links also work.
  if (!(isAbsolute(oldLink))) {
    // if the link starts with a '/'' it should be from the HostName, otherwise it's just from the current href.
    if (oldLink[0] == "/"){
      oldLink = window.location.hostname + oldLink ;
    } else {
      oldLink = window.location.href + oldLink ;
    }
  }

  //https://web.archive.org/web/20170719145939/https://en.wikipedia.org/wiki/Kylix

  // this builds the archive url:
  var newLink = "https://web.archive.org/web/" + currentDate + "/" + oldLink ;

  // this builds the HTML
  var newElementA = "<a class='ArchiveLink' href='" + newLink + "'>&nbsp;💾&nbsp;</a>" ;

  // this inserts the HTML
  baseElement.after( newElementA );
}

function archivePost(baseElement){
  // this handles each threadmark seprately. (For pages with more than one)
  // grab the raw data
  var rawDate = baseElement.children('.messageMeta').find('.DateTime').attr('title');
  var rawTimestamp = baseElement.find('.DateTime').data("time");

  var currentDate = "*";
  if (rawDate === undefined) {
    if (rawTimestamp === undefined) {
      // if there is no data, give the page with all the options
      currentDate = "*";
    } else {
      // if there is not a raw date but there is a unix date, use it.
      currentDate = getUnixDate(rawTimestamp);
    }
  } else {
    // if there is a raw date, use it.
    currentDate = getDate(rawDate);
  }

  // this gets all the links in the current threadmarked post (works even if there is more than one threadmark on a page)
  baseElement.find('.messageContent').find('a').each( function(){archiveAdd($(this), currentDate);} );
}

// this makes the text viewable
function VisiInvisiText(baseElement){
  // if the text is transparent
	baseElement.find('.messageContent').find("[style*='color: transparent']").each( function(){
    // add a class so we can find it
  	$( this ).addClass('ShouldaBeInvisi');
    // make it black
    $( this ).css("color", "black");
    // add a background to differenteate it
    $( this ).css("background-color", "white");
    // add some icons to show that it was invisible.
    $( this ).prepend("<span class='invisiNotify'>👁️‍🗨️</span>");
    $( this ).append("<span class='invisiNotify'>👁️‍🗨️</span>");
  } );
}

// this makes the text invisible again
function InvisiVisiText(baseElement){
  // find the ex-invisible text
  baseElement.find('.messageContent').find(".ShouldaBeInvisi").each( function(){
    // remove the class
  	$( this ).removeClass('ShouldaBeInvisi');
    // make everything transparent again
    $( this ).css("color", "transparent");
    $( this ).css("background-color", "transparent");
    // remove the custom icons
    $( this ).find('.invisiNotify').remove();
  } );
}

// this sets up all toggle systems
function setupToggles() {
  
  // this sets up the archive toggle switch
  $('.toggleArchiveLinks').click(function() {
    if ($(this).data("enabled") == 'yes'){
      // if the links were there, make them go away
      $(this).text("❌");
      $(this).data("enabled", 'no');
      // this removes the archive links
      $(this).parents('.message').find('.messageInfo').find('.messageContent').find('.ArchiveLink').remove();
    } else {
      // if they were not there, put them in.
      $(this).text("✔️");
      $(this).data("enabled", 'yes');
      $(this).parents('.message').children('.messageInfo').each(function() {
        archivePost( $( this ) );
      });
    }
  });
  
  // this sets up the InvisiText toggle
  $('.toggleInvisiText').click(function() {
    if ($(this).data("enabled") == 'yes'){
      // if the text is viewable, hide it.
      $(this).text("❌");
      $(this).data("enabled", 'no');
      // make the text hidden
      $(this).parents('.message').find('.messageInfo').each(function(){
      	InvisiVisiText( $( this ) );
      });
    } else {
      // if the text was not viewable, make it.
      $(this).text("✔️");
      $(this).data("enabled", 'yes');
      // make the text viwable
      $(this).parents('.message').children('.messageInfo').each(function() {
        VisiInvisiText( $( this ) );
      });
    }
  });
}

// this is the initial routine of the userscript.
function main() {
  $('.hasThreadmark').children('.messageInfo').each(function() {
    // add the archive links to post
	archivePost( $(this) );
    // add and set the toggle for archive documents.
    $( this ).parents('.message').find('.messageUserBlock').find('.userText').append("<br />Archive Links<br />💾: <a style='cursor: pointer;' class='toggleArchiveLinks'>✔️</a>");
    $( this ).parents('.message').find('.messageUserBlock').find('.toggleArchiveLinks').data("enabled", 'yes');
    // add and set the toggle for invisi-text.
    $( this ).parents('.message').find('.messageUserBlock').find('.userText').append("<br />InvisiText Viewer<br />👁️‍🗨️: <a style='cursor: pointer;' class='toggleInvisiText'>❌</a>");
    $( this ).parents('.message').find('.messageUserBlock').find('.toggleInvisiText').data("enabled", 'no');
  });
  
  // this adds the option for non-threadmarked posts to get archive links
  $('.message').children('.messageUserInfo').find('.userText').each(function() {
    // if the post does not already have the archive toggle.
  	if ($( this ).find('.toggleArchiveLinks').length === 0){
      // add and set the toggle for archive documents.
      $( this ).append("<br />Archive Links<br />💾: <a style='cursor: pointer;' class='toggleArchiveLinks'>❌</a>");
      $( this ).find('.toggleArchiveLinks').data("enabled", 'no');
      // add and set the toggle for invisi-text.
      $( this ).parents('.message').find('.messageUserBlock').find('.userText').append("<br />InvisiText Viewer<br />👁️‍🗨️: <a style='cursor: pointer;' class='toggleInvisiText'>❌</a>");
      $( this ).parents('.message').find('.messageUserBlock').find('.toggleInvisiText').data("enabled", 'no');
    }
  });
}

// Call the initial routine of the script.
main();

setupToggles();