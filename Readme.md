# What this script does
This script adds a link to archive.org after all other links in threadmarked posts. The link includes a timestamp of when the post was first posted, so it should go to the copy that the writter was viewing. (but you can always change the date in the menu at the top after clicking on the archive link) It can also be disabled or enabled on all posts through the menu in user-info block next to the post.

It also adds a basic invisi-text viewer (default off) that allows posts to be viewed. (again, toggalable in the user-info block)

# How to install.

Requirements:

Grease Monkey or alternative.

To install, open the raw of the script [link](https://gitlab.com/Jcc10/Archive_links-Forum_story/raw/master/Archived_Pages_Links.user.js) and click install. (You may have to enable the script first for it to work.)